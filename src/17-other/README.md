# :mega: 关注公众号：Coder编程

- [跳槽季，金三已过，银四你准备好了吗？](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&tempkey=MTAwNV9pZ0ZkaFlDK3JrQ01iaTVmR0NySkJVRFk5a1YyX1FTbXVOY3JFUXFJY0NtQ3JIYjhPY1Z3eGdNLUZCdTEtZWkwbXdMa0J6alpKUG12US1hWG5saXhKTmJFV09GbEhtQlRDa3YtMUZsWTFkRG9yTUl3UWU5NkZtOWlSRElWYV8xb0t0LXpscGNBSnRJTF9vYXc0U2VUazBsMmpfQTcwa1dRbTlkaE5Bfn4%3D&chksm=16e6703d2191f92b840943fd0fa971f3cb8a399f16a3b855751e314bf4f7d8cedf692112af13#rd)


- [海量精彩学习资料等你来拿！]()


## PMP备考指南系列

- [PMP备考指南之相关事项介绍](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483746&idx=1&sn=05b6bdc1c46e535027ce8c65715c4c30&chksm=96e67071a191f967633b46f7c2f11e1934b7569abe2d74c008e7b076e8bb4281753636a91232&token=1664462808&lang=zh_CN#rd)


- [PMP备考指南之第一章：引论](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483751&idx=1&sn=d25153700d9a167ef6b577ff008b0cc2&chksm=96e67074a191f962c61eadfc7df86264e75430c695af145ca12450af6aff81ea70f13e703fd3&token=267633840&lang=zh_CN#rd)


- [PMP备考指南之第二章：项目运作环境](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483757&idx=1&sn=aa8960891e2f829310e4ba7056e1e53a&chksm=96e6707ea191f96815965ffb1d8b8aa234f19e09addd8f773311d5a35995cdacffc35c77be3b&token=267633840&lang=zh_CN#rd)
 

- [PMP备考指南之第三章：项目经理角色](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483790&idx=1&sn=21d48b52547121878dc10338840116f7&scene=19&token=267633840&lang=zh_CN#wechat_redirect) 


- [PMP备考指南之第四章：项目整合管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483798&idx=1&sn=29a60203e33406b72f5933f111f01e3f&chksm=96e67085a191f993e23fed328b8caf1f761e905bed5f85ccbaa9bf8f47495263cf041e0ba152&token=267633840&lang=zh_CN#rd) 


- [PMP备考指南之第五章：项目范围管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483802&idx=1&sn=cf2f367567d8b0afdfbb28205adbd16c&chksm=96e67089a191f99f41b4bd0cc18bd70b5d91c5486fd91ba08618e93734d949f8d9e396893a30&token=267633840&lang=zh_CN#rd) 


- [PMP备考指南之第六章：项目进度管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483806&idx=1&sn=00628bb912ec96cb0e5700101f751683&chksm=96e6708da191f99b9bb145a76e528ed0d09eed881b5554cb4e54a1f884cfb154b706b698060c&token=267633840&lang=zh_CN#rd) 


- [PMP备考指南之第七章：项目成本管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483810&idx=1&sn=4ba72b234fdd8ea99665db2099c90a0b&chksm=96e670b1a191f9a70a80a8bc267ca2b74857920f4760e601e70b5997262ab3558acb10b3d505&token=267633840&lang=zh_CN#rd) 


- [PMP备考指南之第八章：项目质量管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483814&idx=1&sn=927c309793ec2879683e9638ee5cdfb0&chksm=96e670b5a191f9a3807a859119e3c36ea94d1476de2f195961c5c2cb944c4ef3104ffec720c7&token=267633840&lang=zh_CN#rd) 


- [PMP备考指南之第九章：项目资源管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483824&idx=1&sn=cf5a0b481554d1b9a4c0497062f662b9&chksm=96e670a3a191f9b560ab2a6e18525fd3c68ddc776cacbb1df637b414115ee8825898146056ee&token=267633840&lang=zh_CN#rd) 


- [PMP备考指南之第十章：项目沟通管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483828&idx=1&sn=af8b7b6f8858029abf56c05b1f95cf21&chksm=96e670a7a191f9b1c6732c0ba981bcda6d9f5afc50b9ee074725b935a5dc0982c0d4143d37a9&token=948950272&lang=zh_CN#rd) 


- [PMP备考指南之第十一章：项目风险管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483833&idx=1&sn=c3ea00b633e28bc3e27d2eb7bbfa3e68&chksm=96e670aaa191f9bc9c5f5eda6d2318adada8c7c0a85fe7fa98ad869b22413441af56846192ca&token=948950272&lang=zh_CN#rd) 


- [PMP备考指南之第十二章：项目采购管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483840&idx=1&sn=f9c6f2025d3ee9acc5e542e6aacf5290&chksm=96e670d3a191f9c5c57dbdbc2ff9ef77631769ea6d283a53fad8ab2a1e9e20e7ba40af0c3e43&token=948950272&lang=zh_CN#rd) 


- [PMP备考指南之第十三章：项目干系人管理](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483841&idx=1&sn=d06aa1363f57416363ddd5185e87f74f&chksm=96e670d2a191f9c4b3ec7ede6f16d11c779de1f40a931acb7a8c2284379d905c18539e183d78&token=948950272&lang=zh_CN#rd) 



## OCP 项目

- [强烈推荐一款开源项目！ （OPC）微服务能力开放平台！](https://mp.weixin.qq.com/s?__biz=MzIwMTg3NzYyOA==&mid=2247483820&idx=1&sn=7ef4c4db71a923a2400b079e2692d7c6&scene=19&token=267633840&lang=zh_CN#wechat_redirect) 


